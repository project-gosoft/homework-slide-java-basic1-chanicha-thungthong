package homework;

public class homework3 {
	public static void main(String[] args) {
		// draw9(2);
		// draw10(3);
		// draw11(4);
		// draw12(3);
		// draw13(3);
		// draw14(3);
		// draw15(4);
		// draw16(4);
		draw17(4);
	}

	public static int draw9(int n) {
		for (int i = 0; i < n; i++) {
			int j = i * 2;
			System.out.println(j);
		}
		return n;
	}

	public static int draw10(int n) {
		for (int i = 1; i < n; i++) {
			int j = i * 2;
			System.out.println(j);
		}
		return n;
	}

	public static int draw11(int n) {
		int k = 1;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++, k++) {
				System.out.print(k);
			}
			System.out.println();
		}
		return n;
	}

	public static int draw12(int n) {
		String[] result = new String[n];
		for (int i = 0; i < n; i++)
			result[i] = "*";
		for (int i = 0; i < n; i++) {
			result[i] = "-";
			for (int j = 0; j < n; j++)
				System.out.print(result[j]);
			result[i] = "*";

			System.out.println();
		}
		return n;
	}

	public static int draw13(int n) {
		for (int i = 1; i <= n; i++) {

			for (int j = n; j >= 1; j--) {

				if (j != i)
					System.out.print("*");
				else
					System.out.print("-");
			}
			System.out.println();
		}
		return n;
	}

	public static int draw14(int n) {
		String[] result = new String[n];
		for (int i = 0; i < n; i++)
			result[i] = "-";
		for (int i = 0; i < n; i++) {
			result[i] = "*";
			for (int j = 0; j < n; j++)
				System.out.print(result[j]);
			result[i] = "*";

			System.out.println();
		}
		return n;
	}

	public static int draw15(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = i; j <= n; j++) {
				System.out.print("*");
			}
			for (int j = 1; j <= i - 1; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
		return n;
	}

	public static int draw16(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print("*");
			}
			for (int j = i; j <= n - 2; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
		for (int i = 1; i <= n - 1; i++) {
			for (int j = 1; j <= (n - i); j++) {
				System.out.print("*");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
		return n;
	}

	public static int draw17(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(i + 1);
			}
			for (int j = i; j <= n - 2; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
		for (int i = 1; i <= n - 1; i++) {
			for (int j = 1; j <= (n - i); j++) {
				System.out.print(n - i);
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("-");
			}
			System.out.println();
		}
		return n;
	}
}
