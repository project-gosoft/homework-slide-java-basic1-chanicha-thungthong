package homework;

public class homework2 {
	public static void main(String[] args) {

		multiplyTable();
	}

	public static void multiplyTable() {
		String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
		int[] newtable = new int[table.length];
		for (int row = 0; row < table.length; row++) {
			for (int element = 0; element < table.length; element++) {
				newtable[row] = Integer.parseInt(table[row][element]);
				System.out.printf("%-3d", newtable[row] * 2);

			}
			System.out.println();
		}
	}
}
// ตัวอย่าง
//for (int row = 0; row < twoD_Array.length; row++){
//	   for (int element = 0; element < twoD_Array[row].length; element++){
//	       System.out.println(twoD_Array[row][element]);
//	     }
//	   }
//	 }
//	}
