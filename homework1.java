package homework;

public class homework1 {
	public static void main(String[] args) {

		// อันนี้เป็นการทดลองเฉยๆนะคะ การบ้านจริงๆอยู่อีกไฟล์นะคะ ที่ชื่อว่า homework

		draw1(4);
		// draw2();
		// draw3();
		// draw4();
		// draw5();

	}

	public static void draw1(int n) {
		for (int j = 1; j <= n; j++) {

			System.out.print("*");
		}
		System.out.println();
	}

	public static void draw2() {
		for (int n = 2; n <= 4; n++) {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					System.out.print("*");
				}
				System.out.println();
			}
			System.out.println();
		}
	}

	public static void draw3() {
		for (int n = 3; n <= 5; n++) {
			for (int i = 1; i < n; i++) {
				for (int j = 1; j < n; j++) {
					System.out.print("" + j);
				}
				System.out.println();
			}
			System.out.println();
		}
	}

	public static void draw4() {
		for (int n = 4; n >= 2; n--) {
			for (int i = 0; i < n; i++) {
				for (int j = n; j >= 1; j--) {
					System.out.print(j);
				}
				System.out.println();
			}
			System.out.println();
		}
	}

	public static void draw5() {
		for (int n = 3; n <= 5; n++) {
			for (int j = 1; j < n; j++) {
				for (int i = 2; i <= n; i++) {
					System.out.print("" + j);
				}
				System.out.println();
			}
			System.out.println();
		}
	}

	public static void draw6() {
		for (int n = 3; n <= 5; n++) {
			for (int j = n - 1; j > 0; j--) {
				for (int i = 2; i <= n; i++) {
					System.out.print("" + j);
				}
				System.out.println();
			}
			System.out.println();
		}
	}

//	public static void draw7() {
//		for (int n = 1; n <= 5; n++) {
//			for (int j = n - 1; j > 0; j--) {
//				for (int i = 1; i <= n; i++) {
//					System.out.printf("%3d", i);
//				}
//				System.out.print("\n");
//			}
//
//			System.out.println("");
//		}
//	}
}
