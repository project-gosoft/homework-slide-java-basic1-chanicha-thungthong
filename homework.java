package homework;

import java.util.Scanner;

public class homework {
	public static void main(String[] args) {
		// draw1(5); // ใส่ค่าตรงวงเล็บได้เลยเพื่อเปลี่ยนค่าข้อมูลของตัวแปล n
		// draw2(4);
		// draw3(6);
		// draw4(3);
		// draw5(6);
		// draw6(4);
		// draw7(4);
		 draw8(4);
	}

	public static int draw1(int n) {
		for (int i = 0; i < n; i++) {
			System.out.print("*");
		}
		return n;
	}

	public static int draw2(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
		return n;
	}

	public static int draw3(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
		return n;
	}

	public static int draw4(int n) {
		for (int i = n; i > 0; i--) {
			for (int j = n; j > 0; j--) {
				System.out.print(j);
			}
			System.out.println();
		}
		return n;
	}

	public static int draw5(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				System.out.print(i);
			}
			System.out.println();
		}
		return n;
	}

	public static int draw6(int n) {
		for (int i = n; i >= 1; i--) {
			for (int j = 1; j <= n; j++) {
				System.out.print(i);
			}
			System.out.println();
		}
		return n;
	}

	public static int draw7(int n) {
		int k = 1;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++, k++) {
				System.out.printf("%-3d", k);
			}
			System.out.println();
		}
		return n;
	}

	public static int draw8(double n) {
		double k = Math.pow(n, 2);
		for (double i = n; i >= 1.0; i--) {
			for (double j = n; j >= 1.0; j--, k--) {
				System.out.printf("%-3d", (int) k);
			}
			System.out.println();
		}
		return (int) n;
	}
}
